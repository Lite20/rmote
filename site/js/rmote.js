var video = document.querySelector("#videoElement");

// check for getUserMedia support
navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia || navigator.oGetUserMedia;
 
if (navigator.getUserMedia) {       
    // get webcam feed if available
    navigator.getUserMedia({video: true}, handleVideo, videoError);
}
 
function handleVideo(stream) {
    // if found attach feed to video element
    video.src = window.URL.createObjectURL(stream);
}
 
function videoError(e) {
    // no webcam found - do something
}

qrcode.callback = function(data) {
    console.log(data);
};

var colorThief = new ColorThief();
var v,canvas,context,w,h;
var clr;
document.addEventListener('DOMContentLoaded', function(){
    // when DOM loaded, get canvas 2D context and store width and height of element
    v = document.getElementById('videoElement');
    canvas = document.getElementById('qr-canvas');
    context = canvas.getContext('2d');
    w = canvas.width;
    h = canvas.height;
},false);

setInterval(function() {
    if(v.paused || v.ended) return false;
    context.drawImage(v, 0, 0, w, h);
    qrcode.decode();
}, 50);

